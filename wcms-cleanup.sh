## A file that destroys all the things we used for our project, returning the directory to it's initial state.

## Destroy Lando
DIR=wcms-artifact
PASS=$1
lando destroy -y;
echo "";
echo "*********************************************************************";
echo "";
echo "Destroyed Lando, moving on to check if $DIR directory exists...";

## Remove the wcms directory we built with the landofile.
if [ -d "$DIR" ]; then
    echo "";
    echo "$DIR directory exists, removing."
    echo $PASS | sudo -S chmod -R 777 $DIR
    rm -rf $DIR;
    if [ -d "$DIR" ]; then
      echo "There was an issue removing the $DIR directory. Permissions?";
    fi
else
    echo "$DIR directory does not exist, so there is nothing to remove.";
fi
if [ -d "$DIR" ]; then
    echo "Directory appears to be in a default state."
    echo "Run lando start to create a new instance.";
fi
echo "";
echo "*********************************************************************";
echo "";

