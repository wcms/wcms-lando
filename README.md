# WCMS Lando setup

## **IMPORTANT**: This repo is being minimally maintained. 

This repository contains all you will need to get up and running with Lando and
the WCMS 3.x.

** Note ** As of May 26/21 the rebuild script isn't putting the symlink for pattern lab in the correct spot. Once you have run `lando start` you will need to fix the path for uw_wcms_gesso. 
To fix this manually:
```
cd wcms-artifact/web
rm uw_wcms_gesso
cd ..
ln -s /app/wcms-artifact/web/profiles/uw_base_profile/modules/custom/uw_wcms_gesso uw_wcms_gesso
```
Now when you go to lando url, you will see uw_wcms_gesso directory and a web directory. Going to the uw_wcms_gesso dir will let you see the pattern lab web interface and going to web will let you create a drupal site.

## Setup the Dev environment with Lando

### Requirements

*  Lando (https://lando.dev/)
*  Git (https://git-scm.com/downloads)
*  Docker (https://www.docker.com/products/docker-desktop), though Lando installs the version it needs if it's missing

(There may be others that I am currently missing...)

On a Mac you can add the Lando-mutagen plugin to make things more performant on the Mac. [It can be found here.](https://github.com/francoisvdv/lando-mutagen)

Once installed you need to make sure the "wcms-artifact" directory is on the "excludes" list in .lando.yml file. 

Having this installed (on a Mac at least) makes a build take roughly 8 minutes (as of March 30, 2022).

### Setup

Clone repo (https://git.uwaterloo.ca/wcms/wcms-lando.git) currently (May. 2021), you need to use the dev branch.

```
 git clone https://git.uwaterloo.ca/mnshantz/wcms-lando.git --branch dev
```

Go into the wcms-lando directory.

```
 cd wcms-lando
```

Since you don't need to commit back to the `Lando` repo we can remove that ability.

```
 git remote remove origin
```

Create the lando site:

```
 lando start
```

At this point, all of the pieces have been installed for your site and you can go to the supplied URL to build a new Drupal site.

Go to your site URL (which should be located at  https://wcms.lndo.site/)
Otherwise run
```
 lando info
```
To get the URL for your site. Once there, you should see the WCMS site with the correct header and footer.

If you need to get rid of your Lando instance:

```
 lando destroy -y
```
Also, remember you need to clean up (remove) any files that were created as part
of the `lando start` command, or you can just delete and re-clone the directory. This means you will need to remove the `wcms-artifact/` folder (if you want to complete remove everything.) There is a `wcms-cleanup.sh` file that will run a `lando destroy -y` and delete the wcms-artifact directory if it exists.

```
 ./wcms-cleanup.sh
```

## Using xdebug with Lando and PHPStorm

**Note** This assumes you already have Lando and PHPstorm installed, and that you have downloaded and installed the required Xdebug plugin for your web browser of choice.

Running xdebug in Lando can cause a performance hit. To enable or disable xdebug you need to change run
```
 lando xdebug-on
```
or
```
 lando xdebug-off
```

The default state is currently off.

Once you have you site (re)built, start PHPStorm.

1. Going to Preferences -> Languages & Frameworks -> PHP and make sure that the PHP language level matches the .lando.yml file, currently (May 2020) set to PHP 7.3.

2. Make sure that you have your Project opened in PHPStorm.

3. Add a breakpoint in your desired module.

4. Open your browser and go to the URL of the lando site, if you are using the defaults it should be https://wcms.lndo.site

5. Make sure you've installed the Xdebug plugin for your browser and enable debugging on your lando site.

6. Refresh the page in your browser. The first time you run this you will need to accept the connection in PHPStorm.

7. You should then see the output in the debugging window in PHPStorm.

## Default database info for creating a Drupal site

The defaults for the database connection (database, user and password) is "drupal8" and under advanced, you will need to change "localhost" to "database".
